#encoding=utf8

from ConfigParser import SafeConfigParser
import os


class FeatureSplitter(object):
    ''''''
    def __init__(self):
        self.cfg = SafeConfigParser()
        self.cfg.read('main.cfg')

        self.data_dir = self.cfg.get('main', 'data_dir')

    def split_features(self):

        writer_lst = []
        with open(self.cfg.get('main', 'train_file')) as reader:
            for i, feature_name in enumerate(reader.readline().strip().split(',')[2:]):
                writer_lst.append(open(os.path.join(self.data_dir, feature_name), 'w'))

            for cnt, line in enumerate(reader):
                if cnt % 100000 == 0:
                    print 'splitting at %d' % cnt
                data = line.strip().split(',')
                ad_id = data[0]
                label = data[1]

                for i, feature in enumerate(data[2:]):
                    writer_lst[i].write('%s\001%s\001%s\n' % (ad_id, label, feature))

            for wt in writer_lst:
                wt.close()


if __name__ == '__main__':

    splitter = FeatureSplitter()
    splitter.split_features()

