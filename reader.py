# encoding=utf8

from collections import Iterator
import os
from ConfigParser import SafeConfigParser

ONE = '1'
ZERO = '0'


def normalize(x, y):
    return ONE if x else ZERO


class FeatureValueReader(Iterator):
    def __init__(self, feature, target_idx=2, func=None):
        """
        @:parameter feature, the target feature to be read
        @:parameter target_idx, the column index in each line
        @:parameter normalize_func, the callback function of scaling the feature value
        """
        cfg = SafeConfigParser()
        cfg.read('main.cfg')
        self.normalize_func = normalize if not func else func
        self.name = feature
        self.data_dir = cfg.get('main', 'data_dir')
        self.feature_sample_file = os.path.join(self.data_dir, feature)
        self.reader = open(self.feature_sample_file, 'r')
        self.target_idx = target_idx
        self.value_dic = self._init_values(feature)
        self.value_buffer = [ZERO] * len(self.value_dic)
        self.last_idx = 0

    def _init_values(self, feature):
        values_dic = {}
        with open(os.path.join(self.data_dir, '%s.val' % feature)) as r:
            for i, line in enumerate(r):
                values_dic[line.strip().partition('\001')[0]] = i

        return dict([(k, v) for k, v in sorted(values_dic.iteritems(), key=lambda x: x[0])])

    def next(self):
        line = self.reader.readline()
        if not line:
            raise StopIteration

        f_val = line.strip().split('\001')[self.target_idx]
        self.value_buffer[self.last_idx] = ZERO
        self.last_idx = self.value_dic[f_val]

        self.value_buffer[self.last_idx] = self.normalize_func(f_val, self.value_dic)

        return self.value_buffer

    def close(self):
        self.reader.close()
