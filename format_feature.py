# encoding=utf8

from ConfigParser import SafeConfigParser
from multiprocessing import Process
from reader import FeatureValueReader
import os

FILTER_FEATURES = ['C19', 'C20', 'C21']
# FILTER_FEATURES = ['app_category', 'app_domain', 'banner_pos', 'device_model', 'device_type', 'hour', 'site_category',
#                   'C1', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21']
cfg = SafeConfigParser()
cfg.read('main.cfg')


def format_value(feature):
    feature_reader = FeatureValueReader(feature, target_idx=2, func=None)
    data_dir = cfg.get('main', 'data_dir')
    sample_reader = open(cfg.get('main', 'train_file'), 'r')
    writer = open(os.path.join(data_dir, '%s.fmt' % feature), 'w')
    sample_reader.readline()  # skip the title
    i = 0
    while True:
        if i % 500000 == 0:
            print 'Process %s at %s' % (feature_reader.name, i)
        label = sample_reader.readline().strip().split(',')[1]
        f_values = feature_reader.next()
        writer.write('%s %s\n' % (label, ' '.join(f_values)))
        i += 1

    writer.close()
    sample_reader.close()
    feature_reader.close()


def run_format():
    features = [f.strip()
                for f in cfg.get('main', 'features').split(',') if f in FILTER_FEATURES]
    processes = []

    for i in xrange(0, len(features) + 6, 6):
        for feature in features[i:i + 6]:
            p = Process(target=format_value, args=(feature,))
            processes.append(p)

        for p in processes:
            p.start()

        for p in processes:
            p.join()


if __name__ == '__main__':
    run_format()
