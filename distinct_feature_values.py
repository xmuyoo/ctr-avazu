# encoding=utf8

from ConfigParser import SafeConfigParser
from multiprocessing import Process
import os
from collections import Counter


class DistinctCounter(object):

    STEP_LENGTH = 4

    def __init__(self):
        self.cfg = SafeConfigParser()
        self.cfg.read('main.cfg')
        self.source_features = [f.strip() for f in self.cfg.get('main', 'features').split(',')]
        self.data_dir = self.cfg.get('main', 'data_dir')

    def distinct(self):

        for i in xrange(0, len(self.source_features), DistinctCounter.STEP_LENGTH):
            print i
            processes = []
            for feature in self.source_features[i:i + DistinctCounter.STEP_LENGTH]:
                p = Process(target=self.count, args=(feature,))
                processes.append(p)

            for p in processes:
                p.start()

            for p in processes:
                p.join()

    def count(self, feature):

        fname = os.path.join(self.data_dir, feature)
        value_counter = {}
        with open(fname, 'r') as reader:
            for i, line in enumerate(reader):
                if i % 500000 == 0:
                    print 'Process %s at %d' % (feature, i)

                try:
                    _ad_id, label, f_val = line.strip().split('\001')
                    if f_val not in value_counter:
                        value_counter[f_val] = Counter()
                    value_counter[f_val][label] += 1
                except BaseException as e:
                    print 'Error at %d of Process %s' % (i + 1, feature)
                    continue

        writer = open(os.path.join(self.data_dir, '%s.val' % feature), 'w')
        for f_val, counter in sorted(value_counter.iteritems(), key=lambda x: x[0]):
            line = '%s\001%s\001%s\001%s\001%s\n' % (f_val, 1, counter['1'], 0, counter['0'])
            writer.write(line)
        writer.close()


if __name__ == '__main__':
    counter = DistinctCounter()
    counter.distinct()
